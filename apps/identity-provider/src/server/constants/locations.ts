import { join } from 'path';

export const VIEWS_DIR = join(__dirname, '..', '/views');
export const ACCOUNTS_ROUTE = '/account';
