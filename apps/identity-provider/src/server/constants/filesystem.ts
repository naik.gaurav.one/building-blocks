import { join } from 'path';

export const SERVER_MAIN_JS = join(process.cwd(), 'dist', 'server', 'main');
