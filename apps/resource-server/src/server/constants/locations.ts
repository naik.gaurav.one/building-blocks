import { join } from 'path';

export const ROOT_ROUTE = '/';
export const PUBLIC_DIR = join(__dirname, '..', '/public');
export const VIEWS_DIR = join(__dirname, '..', '/views');
export const QUASAR_DIR = join(__dirname, '..', '..', '..', '/dist/spa-mat');
